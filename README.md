# Example REST API for the teamwork

This repository contains an Express application implementing a very simple REST API for a book store. It is used to illustrated the following key technologies:

 - REST API.
 - Express and its middlewares.
 - Testing with Mocha.
 - JWT (_Json Web Token_).
 - Using Sequelize to access a database.
 - Documenting a REST API with Swagger.
 
# Repository organization

The repository is organized in several directories:

 - `/app`: contains the code of the express application.
 
    - `app/config`: contains the configuration files (database and authentication).
    - `app/routes`: contains the API endpoints exposed by the server.
    - `app/middlewares/validatos`: contains the validators for the API endpoints.
    - `app/controllers`: contains the controllers for the API endpoints.
    - `app/models`: contains the data models.
   
 - `/test`: contains the code of the tests.

# Usage

You must follow these steps to deploy the application in a remote machine:
 
 1. `sudo docker-compose up`

You must follow these steps to start the application in a local machine:

 1. Deploy the MySQL server in a remote machine:
   
    - `cd database_test`
    - `sudo docker-compose up`
 
 2. Configure the `.env` file in the local machine with the IP of the remote machine.
 
 3. Install dependencies in the local machine:
  
    - `npm install`

 4. Run the application in the local machine with the desired configuration:
 
    - `npm run dev` to run the server in development mode.
    - `npm start` to run the server in production mode.
    - `npm test` to run the tests.